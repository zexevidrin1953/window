import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:27:01.880468
#2018-02-27 09:43:01.721758
#2018-02-27 09:58:02.130562
#2018-02-27 10:25:01.512412
#2018-02-27 10:47:01.786259
#2018-02-27 11:09:01.490388
#2018-02-27 11:38:01.452991
#2018-02-27 12:00:03.558697
#2018-02-27 12:23:01.470632
#2018-02-27 12:51:01.972805
#2018-02-27 13:18:01.409179
#2018-02-27 13:34:01.800000
#2018-02-27 14:04:02.177560
#2018-02-27 14:19:01.756969
#2018-02-27 14:48:01.765230
#2018-02-27 15:07:01.851196
#2018-02-27 15:32:01.275598
#2018-02-27 15:51:02.018469
#2018-02-27 16:17:01.641002
#2018-02-27 16:37:01.213298
#2018-02-27 16:54:02.029518
#2018-02-27 17:47:01.532793
#2018-02-27 18:46:01.973291
#2018-02-27 19:31:01.916254
#2018-02-27 20:35:04.112117